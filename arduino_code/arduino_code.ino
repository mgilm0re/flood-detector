//source: https://www.circuitbasics.com/how-to-send-texts-with-an-arduino/

#include "SerialTransfer.h"

#define CLEAR     4
#define CAUTION  15
#define WARNING  30
#define CRITICAL 100

// enum for FSM states
enum waterStates {
     clear_state,
   caution_state,
   warning_state,
  critical_state,
   unknown_state
};

// constants
const unsigned int offset = 100;

// defines pins
const unsigned char echoPin   = 2;
const unsigned char trigPin   = 3;
const unsigned char serialPin = 1;
const unsigned char txPin     = 1;

// global variable
unsigned char    height;
enum waterStates state;
unsigned int     status;
SerialTransfer   myTransfer;


// prototypes for sonar
void  init_height ();
void   get_height ();
void print_height (); // for testing purposes

// prototypes for state
void   set_state();
void print_state(); // for testing purposes

// prototypes for serial communication
void init_serial();

void setup()
{
  init_height();
  init_serial();
}

void loop()
{
    get_height();
  print_height(); // for testing purposes

    set_state();
  print_state(); // for testing purposes
  
  
  status = digitalRead(serialPin);
  myTransfer.txObj(status, sizeof(status));
  myTransfer.sendData(sizeof(status));
  delay(100);
}


void init_height() {
  
  // initialize sonar sensor pins
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an OUTPUT
  pinMode(echoPin,  INPUT); // Sets the echoPin as an  INPUT
}

void get_height() {
  
  // samples the sensor
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);

  // Calculating the distance in cm of soundwave
  // gets time for sound wave then multiplys by speed of sound
  // then divides by 2 for time to water surface then back.
  float distance = pulseIn(echoPin, HIGH) * 0.034 / 2;

  // calculates height of water in cm.
  height = int(offset - distance);
}

void print_height() {
  
  // Displays the distance on the Serial Monitor
  Serial.print("water level: ");
  Serial.print(height);
  Serial.println(" cm");  
}

void set_state() {
  if (height <= CLEAR)
    state = clear_state;
  else if (height <= CAUTION)
    state = caution_state;
  else if (height <= WARNING)
    state = warning_state;
  else if (height <= CRITICAL)
    state = critical_state;
  else
    state = unknown_state;
  
}

void print_state() {
  switch (state) {
    case clear_state:
      Serial.println("In clear_state");
      break;
    case caution_state:
      Serial.println("In caution_state");
      break;
    case warning_state:
      Serial.println("In warning_state");
      break;
    case critical_state:
      Serial.println("In critical_state");
      break;
    case unknown_state:
      Serial.println("WARNING: In an unknown stae");
      break;
  }
}

void init_serial() {
  Serial.begin(115200);
  myTransfer.begin(Serial);
  pinMode(txPin,OUTPUT);
}

//void send_height() {
//  
//}
