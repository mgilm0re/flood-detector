#include "ESP8266WiFi.h"
  
const char* ssid     = "null"; //Enter SSID
const char* password = "null"; //Enter Password

int    HTTP_PORT   = 80;
String HTTP_METHOD = "GET"; // or "POST"
char   HOST_NAME[] = "maker.ifttt.com"; // hostname of web server:
String PATH_NAME   = "/trigger/test/with/key/b1xFxbqt74ajqCTf6NUrZU";

void setup(void)
{ 
  Serial.begin(115200);
  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) 
  {
     delay(500);
     Serial.print("*");
  }
  
  Serial.println("");
  Serial.println("WiFi connection Successful");
  Serial.print("The IP Address of ESP8266 Module is: ");
  Serial.println(WiFi.localIP());// Print the IP address

  WiFiClient client;

  if(client.connect(HOST_NAME, HTTP_PORT)) {
    Serial.println("Connected to server");

    // send HTTP request header
    client.println(HTTP_METHOD + " " + PATH_NAME + " HTTP/1.1");
    client.println("Host: " + String(HOST_NAME));
    client.println("Connection: close");
    client.println(); // end HTTP request header
  } else {
    Serial.println("connection failed");
  }

}

void loop() 
{

  
  // EMPTY
}
